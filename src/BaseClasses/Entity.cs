﻿using BlockCSharp.Vector;

namespace BlockCSharp.BaseClasses
{
    public abstract class Entity
    {
        public Vector3 Direction = new Vector3();
        public Vector3 Position = new Vector3();
        public Vector3 Right = new Vector3();
        public Vector3 Rotation = new Vector3();
        public Vector3 Up = new Vector3();

        public abstract void Start(Vector3 entityPosition, Vector3 entityRotation);
        public abstract void Update(Block updater);

        public abstract void Translate(Vector3 moveVector);
        public abstract void Rotate(Vector3 rotateVector);
    }
}