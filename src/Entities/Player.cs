﻿using System;
using BlockCSharp.Attributes;
using BlockCSharp.BaseClasses;
using BlockCSharp.GUIs;
using BlockCSharp.Interfaces;
using OpenTK;
using OpenTK.Input;
using Vector3 = BlockCSharp.Vector.Vector3;

namespace BlockCSharp.Entities
{
    [Entity(SRegistryKey = "blocksharp:player")]
    public class Player : Entity, ITickable
    {
        public IScreen CurrentScreen;
        public Matrix4 ProjectionMatrix;

        public Matrix4 ViewMatrix;
        public Matrix4 ViewProjectionModelMatrix;

        public void Tick()
        {
        }

        public override void Translate(Vector3 moveVector)
        {
            Position += moveVector;

            UpdateViewMatrix();
        }

        public override void Rotate(Vector3 rotateVector)
        {
            Rotation += rotateVector;

            if (Rotation.X > 360) Rotation.X -= 360;
            if (Rotation.Y > 360) Rotation.Y -= 360;
            if (Rotation.Z > 360) Rotation.Z -= 360;

            if (Rotation.X < -360) Rotation.X += 360;
            if (Rotation.Y < -360) Rotation.Y += 360;
            if (Rotation.Z < -360) Rotation.Z += 360;

            UpdateViewMatrix();
        }

        public override void Start(Vector3 entityPosition, Vector3 entityRotation)
        {
            UpdateViewMatrix();

            var inWorldGui =
                Registries.ScreenRegistry.Get(new RegistryKey("blocksharp:in_world_gui")) as InWorldGUI;
            inWorldGui.KeyDictionary.Add(Key.W, OnKeyDown);
            inWorldGui.KeyDictionary.Add(Key.A, OnKeyDown);
            inWorldGui.KeyDictionary.Add(Key.S, OnKeyDown);
            inWorldGui.KeyDictionary.Add(Key.D, OnKeyDown);
            inWorldGui.KeyDictionary.Add(Key.Up, OnKeyDown);
            inWorldGui.KeyDictionary.Add(Key.Left, OnKeyDown);
            inWorldGui.KeyDictionary.Add(Key.Down, OnKeyDown);
            inWorldGui.KeyDictionary.Add(Key.Right, OnKeyDown);
        }

        public override void Update(Block updater)
        {
        }

        public void UpdateViewMatrix()
        {
            Console.WriteLine(Position);
            Console.WriteLine(Rotation);

            Direction = new Vector3(
                (float) Math.Cos(MathHelper.DegreesToRadians(Rotation.Z)) *
                (float) Math.Sin(MathHelper.DegreesToRadians(Rotation.Y)),
                (float) Math.Sin(MathHelper.DegreesToRadians(Rotation.Z)),
                (float) Math.Cos(MathHelper.DegreesToRadians(Rotation.Z)) *
                (float) Math.Cos(MathHelper.DegreesToRadians(Rotation.Y)));

            Right = new Vector3((float) Math.Sin(MathHelper.DegreesToRadians(Rotation.Y) - Math.PI / 2.0d),
                0,
                (float) Math.Cos(MathHelper.DegreesToRadians(Rotation.Y) - Math.PI / 2.0d));

            Up = OpenTK.Vector3.Cross(Right, Direction);

            ViewMatrix = Matrix4.LookAt(Position, Position + Direction, Up);
            ProjectionMatrix =
                Matrix4.CreatePerspectiveFieldOfView(MathHelper.DegreesToRadians(90.0f), 4.0f / 3.0f, 0.1f, 100.0f);

            ViewProjectionModelMatrix = Matrix4.Identity * ViewMatrix * ProjectionMatrix;
        }

        public int OnKeyDown(KeyboardKeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.W:
                    Translate(Direction * 0.1f);
                    break;
                case Key.A:
                    Translate(Right * -0.1f);
                    break;
                case Key.S:
                    Translate(Direction * -0.1f);
                    break;
                case Key.D:
                    Translate(Right * 0.1f);
                    break;
                case Key.Up:
                    Rotate(new Vector3(0, 0, 1.0f));
                    break;
                    ;
                case Key.Left:
                    Rotate(new Vector3(0, 1.0f, 0));
                    break;
                case Key.Down:
                    Rotate(new Vector3(0, 0, -1.0f));
                    break;
                case Key.Right:
                    Rotate(new Vector3(0, -1.0f, 0));
                    break;
                default:
                    break;
            }

            return 0;
        }
    }
}