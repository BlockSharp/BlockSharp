﻿using System;
using BlockCSharp.Vector;

namespace BlockCSharp.Helpers
{
    public static class MathHelper
    {
        public static Vector3 NormalFromAngles(float angleX, float angleY)
        {
            return new Vector3((float) Math.Cos(angleY) * (float) Math.Sin(angleX),
                (float) Math.Cos(angleY) * (float) Math.Cos(angleX), (float) Math.Sin(angleX));
        }
    }
}