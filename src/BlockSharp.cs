﻿using System;
using BlockCSharp.BaseClasses;
using BlockCSharp.Entities;
using BlockCSharp.GUIs;
using BlockCSharp.Renderers;
using BlockCSharp.Vector;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Input;
using Vector3 = BlockCSharp.Vector.Vector3;

namespace BlockCSharp
{
    public class BlockSharp : GameWindow
    {
        private readonly Player _localPlayer = new Player();

        private Renderer _renderer;

        public BlockSharp() : base(640, // initial width
            480, // initial height
            GraphicsMode.Default,
            "BlockCSharp", // initial title
            GameWindowFlags.Default,
            DisplayDevice.Default,
            4, // OpenGL major version
            4, // OpenGL minor version
            GraphicsContextFlags.ForwardCompatible)
        {
        }

        protected override void OnLoad(EventArgs e)
        {
            _renderer = new OpenGLRenderer(_localPlayer);
            _renderer.Setup();

            Initialization.Start();

            _localPlayer.Start(new Vector3(0, 0, 0), new Vector3(0, 0, 0));
            _localPlayer.CurrentScreen =
            Registries.ScreenRegistry.Get(new RegistryKey("blocksharp:in_world_gui")) as InWorldGUI;

            World.World.AddChunk(new Vector2i(0, 0));
            World.World.AddChunk(new Vector2i(-1, 0));

            World.World.SetBlock(new Vector3i(0, 0, 0),
                Registries.BlockRegistry.Get(new RegistryKey("blocksharp:test_block")));
        }

        protected override void OnRenderFrame(FrameEventArgs e)
        {
            _renderer.RenderFrame();

            SwapBuffers();

            base.OnRenderFrame(e);
        }

        protected override void OnUpdateFrame(FrameEventArgs e)
        {
            if (World.World.ChunkUpdateQueue.Count != 0)
                World.World.ChunkUpdateQueue.Pop().ChunkUpdate();

            base.OnUpdateFrame(e);
        }

        protected override void OnKeyDown(KeyboardKeyEventArgs e)
        {
            if (_localPlayer.CurrentScreen.KeyDictionary.ContainsKey(e.Key))
                _localPlayer.CurrentScreen.KeyDictionary[e.Key](e);
        }

        protected override void OnKeyUp(KeyboardKeyEventArgs e)
        {
            if (_localPlayer.CurrentScreen.KeyDictionary.ContainsKey(e.Key))
                _localPlayer.CurrentScreen.KeyDictionary[e.Key](e);
        }
    }
}