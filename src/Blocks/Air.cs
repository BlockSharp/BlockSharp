﻿using BlockCSharp.Attributes;
using BlockCSharp.BaseClasses;
using BlockCSharp.Vector;

namespace BlockCSharp.Blocks
{
    [Block(SRegistryKey = "blocksharp:air")]
    public class Air : Block
    {
        public override void Start(Vector3i blockPosition)
        {
        }

        public override void Update(Block updater)
        {
        }
    }
}