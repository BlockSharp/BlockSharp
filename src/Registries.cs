﻿using System.Collections.Generic;
using System.Reflection;
using BlockCSharp.Attributes;
using BlockCSharp.BaseClasses;
using BlockCSharp.ModLoader;
using BlockCSharp.Interfaces;

namespace BlockCSharp
{
    public static class Registries
    {
        public static TypeRegistry<Block, BlockAttribute> BlockRegistry = new TypeRegistry<Block, BlockAttribute>();

        public static TypeRegistry<Entity, EntityAttribute>
            EntityRegistry = new TypeRegistry<Entity, EntityAttribute>();

        public static ObjectRegistry<IScreen, ScreenAttribute> ScreenRegistry =
            new ObjectRegistry<IScreen, ScreenAttribute>();
        
        public static Dictionary<string, Assembly> AssemblyRegistry = new Dictionary<string, Assembly>();
        
        public static Dictionary<string, Mod> ModRegistry = new Dictionary<string, Mod>();
    }
}