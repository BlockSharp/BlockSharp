﻿using System;

namespace BlockCSharp.ModLoader
{
    public class Dependency
    {
        public Version MinimalVersion;
        public Version MaximumVersion;

        public string InternalName;
    }
}