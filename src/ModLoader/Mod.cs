﻿using System;
using System.Collections.Generic;

namespace BlockCSharp.ModLoader
{
    public abstract class Mod
    {
        public string InternalName;
        public string DisplayName;
        public Version Version;

        public List<Dependency> Dependencies;

        public abstract void PreInit();
        public abstract void Init();
        public abstract void PostInit();
    }
}