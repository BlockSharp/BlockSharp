﻿using System;
using System.IO;
using System.Reflection;

namespace BlockCSharp.ModLoader
{
    public static class Loader
    {
        public static void Begin(string directory)
        {
            if (!Directory.Exists(directory))
                Directory.CreateDirectory(directory);
            
            string[] files = Directory.GetFiles(directory);

            foreach (var file in files)
            {
                _loadAssembly(file);
            }
        }

        private static void _loadAssembly(string file)
        {
            Assembly assembly = Assembly.LoadFrom(file);

            Registries.AssemblyRegistry.Add(file, assembly);
            
            Type[] types = assembly.GetExportedTypes();
            
            Console.WriteLine(types[0].FullName, types[1].FullName);

            foreach (var type in types)
            {
                if (typeof(Mod).IsAssignableFrom(type) && !type.IsAbstract)
                {
                    Mod mod = (Mod)Activator.CreateInstance(type);
                    
                    Registries.ModRegistry.Add(mod.InternalName, mod);
                }
            }
        }
    }
}