﻿namespace BlockCSharp
{
    public class RegistryKey
    {
        public string Name;
        public string Namespace;

        public RegistryKey(string compressedName)
        {
            var split = compressedName.Split(':');

            if (split.Length == 2)
            {
                Namespace = split[0];
                Name = split[1];
            }
            else
            {
                Namespace = "null";
                Name = "null";
            }
        }

        public RegistryKey(string nameSpace, string name)
        {
            Namespace = nameSpace;
            Name = name;
        }

        public string GetCompressedName()
        {
            return Namespace + ":" + Name;
        }

        protected bool Equals(RegistryKey other)
        {
            return string.Equals(Name, other.Name) && string.Equals(Namespace, other.Namespace);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((RegistryKey) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((Name != null ? Name.GetHashCode() : 0) * 397) ^ (Namespace != null ? Namespace.GetHashCode() : 0);
            }
        }

        public override string ToString()
        {
            return GetCompressedName();
        }
    }
}