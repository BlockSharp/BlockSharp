﻿using System.Collections.Generic;
using System.Reflection;
using BlockCSharp.ModLoader;

namespace BlockCSharp
{
    public static class Initialization
    {
        public static void Start()
        {
            _loadMods();
            _populateRegistries();

            _preInit();
            _init();
            _postInit();
        }

        private static void _loadMods()
        {
            Loader.Begin("./mods");
        }
        
        private static void _populateRegistries()
        {
            List<Assembly> assemblies = new List<Assembly> { Assembly.GetCallingAssembly() };

            foreach (var mod in Registries.ModRegistry.Values)
            {
                assemblies.Add(Assembly.GetAssembly(mod.GetType()));
            }
            
            Registries.BlockRegistry.Populate(assemblies.ToArray());
            Registries.EntityRegistry.Populate(assemblies.ToArray());
            Registries.ScreenRegistry.Populate(assemblies.ToArray());
        }

        private static void _preInit()
        {
            foreach (var mod in Registries.ModRegistry.Values)
            {
                mod.PreInit();
            }
        }

        private static void _init()
        {
            foreach (var mod in Registries.ModRegistry.Values)
            {
                mod.Init();
            }
        }

        private static void _postInit()
        {
            foreach (var mod in Registries.ModRegistry.Values)
            {
                mod.PostInit();
            }
        }
    }
}